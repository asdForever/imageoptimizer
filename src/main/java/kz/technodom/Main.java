package kz.technodom;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Main {

    private static final float compressionLevel = 0.9f;

    private static long totalNumberOfImages = 0;
    private static long totalNumberOfIgnoredImages = 0;
    private static long totalNumberOfRevertedImages = 0;
    private static long totalNumberOfCompressedImages = 0;

    private static long originalFilesLength = 0;
    private static long compressedFilesLength = 0;

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.err.println("you should specify root folder");
            return;
        }

        final long startTime = System.currentTimeMillis();

        findAndDelete(args[0]);
        findAndCompress(args[0]);

        final long endTime = System.currentTimeMillis();
        final double compressionPercentage = 100 - compressedFilesLength * 100 / originalFilesLength;

        System.out.println("Total number of images: " + totalNumberOfImages);
        System.out.println("Number of ignored images: " + totalNumberOfIgnoredImages);
        System.out.println("Number of reverted images: " + totalNumberOfRevertedImages);
        System.out.println("Number of compressed images: " + totalNumberOfCompressedImages);
        System.out.println("Images size before optimization: " + originalFilesLength);
        System.out.println("Images size after optimization: " + compressedFilesLength);
        System.out.println("Optimization in %: " + compressionPercentage);
        System.out.println("Optimization total time in millis: " + (endTime - startTime));
    }

    private static void findAndCompress(final String baseDirectory) {
        File folder = new File(baseDirectory);
        File[] listOfFiles = folder.listFiles();

        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                if (file.isFile()) {
                    compress(file.getAbsolutePath());
                } else if (file.isDirectory()) {
                    findAndCompress(file.getAbsolutePath());
                }
            }
        }
    }

    private static void compress(final String fullPathToFile) {
        try {
            final File input = new File(fullPathToFile);
            final BufferedImage image = ImageIO.read(input);
            final String outputFileName = fullPathToFile.substring(0, fullPathToFile.length() - 4) + "_" + compressionLevel + ".jpg";
            final File compressedImageFile = new File(outputFileName);

            if (!isImageInJPEG(input)) {
                return;
            }

            totalNumberOfImages++;

            if (!isImageTypeIsForCompression(image)) {
                totalNumberOfIgnoredImages++;
                return;
            }

            System.out.println("file name = " + input.getAbsolutePath());

            originalFilesLength += input.length();

            final OutputStream os = new FileOutputStream(compressedImageFile);

            final JPEGImageWriteParam jpegParams = new JPEGImageWriteParam(null);
            jpegParams.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            jpegParams.setProgressiveMode(ImageWriteParam.MODE_DEFAULT);
            jpegParams.setCompressionQuality(compressionLevel);
            jpegParams.setOptimizeHuffmanTables(true);

            final ImageWriter jpgWriter = ImageIO.getImageWritersByFormatName("jpg").next();

            final ImageOutputStream ios = ImageIO.createImageOutputStream(os);
            jpgWriter.setOutput(ios);

            jpgWriter.write(null, new IIOImage(image, null, null), jpegParams);

            os.close();
            ios.close();
            jpgWriter.dispose();

            if (isNewFileLarger(input, compressedImageFile)) {
                deleteFile(compressedImageFile);
                totalNumberOfRevertedImages++;
            } else {
                Files.copy(
                        Paths.get(compressedImageFile.getAbsolutePath()),
                        Paths.get(input.getAbsolutePath()),
                        StandardCopyOption.REPLACE_EXISTING);
                deleteFile(compressedImageFile);
                totalNumberOfCompressedImages++;
            }

            compressedFilesLength += input.length();
        } catch (IllegalArgumentException | IOException | NullPointerException | IndexOutOfBoundsException e) {
            System.err.println(e);
        }
    }

    private static boolean isNewFileLarger(final File input, final File compressed) {
        return input.length() < compressed.length();
    }

    private static boolean isImageTypeIsForCompression(final BufferedImage image) {
        return isMainCarouselBanner(image) ||
                isPromorotatorBanner(image) ||
                isHorizontalCarouselBanner(image) ||
                isProductImage(image) ||
                isProductImageMedium(image) ||
                isProductImageSmall(image);
    }

    private static boolean isImageInJPEG(final File file) {
        return file.getName().endsWith(".jpg");
    }

    /**
     *
     * @param image 400x400
     * @return true if image size is matched
     */
    private static boolean isProductImage(final BufferedImage image) {
        return image.getWidth() == 400 && image.getHeight() == 400;
    }

    /**
     *
     * @param image 280x450
     * @return true if image size is matched
     */
    private static boolean isPromorotatorBanner(final BufferedImage image) {
        return image.getWidth() == 280 && image.getHeight() == 450;
    }

    /**
     *
     * @param image 740x370
     * @return true if image size is matched
     */
    private static boolean isMainCarouselBanner(final BufferedImage image) {
        return image.getWidth() == 740 && image.getHeight() == 370;
    }

    /**
     *
     * @param image 455x240
     * @return true if image size is matched
     */
    private static boolean isHorizontalCarouselBanner(final BufferedImage image) {
        return image.getWidth() == 455 && image.getHeight() == 240;
    }

    /**
     *
     * @param image 96x96
     * @return true if image size is matched
     */
    private static boolean isProductImageSmall(final BufferedImage image) {
        return image.getWidth() == 96 && image.getHeight() == 96;
    }

    /**
     *
     * @param image 130x130
     * @return true if image size is matched
     */
    private static boolean isProductImageMedium(final BufferedImage image) {
        return image.getWidth() == 130 && image.getHeight() == 130;
    }

    private static void findAndDelete(final String baseDirectory) {
        File folder = new File(baseDirectory);
        File[] listOfFiles = folder.listFiles();

        if (listOfFiles == null) {
            System.out.println("There is no files in: " + folder.getAbsolutePath());
            return;
        }

        for (File file : listOfFiles) {
            if (file.exists() && file.isFile()) {
                deleteFile(file);
            } else if (file.isDirectory()) {
                findAndDelete(file.getAbsolutePath());
            }
        }
    }

    private static void deleteFile(final File file) {
        if (file.exists() && file.isFile() && file.getName().endsWith("_" + compressionLevel + ".jpg")) {
            if (!file.delete()) {
                System.err.println("Failing to remove file " + file.getAbsolutePath());
            }
        }
    }
}
